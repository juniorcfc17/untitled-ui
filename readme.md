
# Projeto Next.js com Tailwind CSS

Este é um projeto simples construído com Next.js e Tailwind CSS. O objetivo principal deste projeto é fornecer uma base para treinar e aprimorar as habilidades de estilização usando o Tailwind CSS.

## 🚀 Como Iniciar

Siga as instruções abaixo para começar a usar o projeto em sua máquina local.

### Pré-requisitos

- **Node.js:** Certifique-se de ter o Node.js instalado. [Download aqui](https://nodejs.org/)
- Neste projeto utilizei o gerenciador de pacotes **PNPM**

### 🛠️ Instalação

1. **Clone este repositório:** 

   ```bash
   git clone https://gitlab.com/juniorcfc17/untitled-ui.git
   ```

2. **Navegue até o diretório do projeto:**

   ```bash
   cd untitled-ui
   ```

3. **Instale as dependências:**

   ```bash
   pnpm install
   ```

### 🌐 Execução Local

Após a instalação, inicie o servidor de desenvolvimento:

```bash
pnpm run dev
```

Abra seu navegador e acesse [http://localhost:3000](http://localhost:3000) para ver o projeto em execução.

### 📂 Estrutura do Projeto

- `src/pages/`: Contém os arquivos das páginas do Next.js.
- `components/`: Componentes reutilizáveis.
- `tailwind.config.js/`: Arquivo para configurações de estilos Tailwind CSS.

### 🎨 Personalização

Sinta-se à vontade para personalizar o projeto de acordo com suas necessidades. Explore e modifique os componentes conforme necessário.

## 🤝 Contribuição

Se você quiser contribuir para este projeto, siga estas etapas:

1. Faça um fork do repositório.
2. Crie uma branch para sua modificação: `git checkout -b feature/nova-feature`.
3. Faça as alterações desejadas.
4. Faça o commit das alterações: `git commit -m 'Adiciona nova feature'`.
5. Faça o push para a branch: `git push origin feature/nova-feature`.
6. Abra um Pull Request.

## 📄 Licença

Este projeto é licenciado sob a Licença MIT - consulte o arquivo [LICENSE](LICENSE) para obter detalhes.
```