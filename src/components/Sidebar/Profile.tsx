import { LogOut } from "lucide-react";
import { Button } from "../Button";

export default function Profile() {
  return (
    <div className="grid items-center gap-3 grid-cols-profile">
      <img src="https://github.com/adilsonjrs.png" className="h-10 w-10 rounded-full" alt="" />
      <div className="flex flex-col truncate">
        <span className="text-sm font-semibold text-zinc-700 dark:text-zinc-100">Adilson Junior</span>
        <span className="text-sm text-zinc-500 truncate dark:text-zinc-400">juniorcfc17@gmail.com</span>
      </div>
      <Button variant="ghost" type="button" className="ml-auto p-2 hover:bg-zinc-50 rounded-md">
        <LogOut className="w-5 h-5 text-zinc-500" />
      </Button>
    </div>
  )
}
